<?php
	include("../inc/simple_html_dom.php");
	include("../inc/entry.php");
	$url = 'http://www.wige-livetiming.de/vln/IDM.HTML'; 
	// $url = 'http://46.38.242.158/vln/livetiming.php';
	$html = file_get_html($url); 
	$rows = $html->find("table[id=AutoNumber1] tbody tr");
	$res = array();
	for($i=0;$i<count($rows);$i++) {
		if($i != 0) {
			$rchildren = $rows[$i]->children();
			
			$tmp = new entry;
			$tmp->position = intval($rchildren[0]->plaintext);
			$tmp->number = $rchildren[1]->plaintext;
			$tmp->name = $rchildren[2]->plaintext;
			$tmp->laps = intval($rchildren[3]->plaintext);
			$tmp->gap = $rchildren[4]->plaintext;
			$tmp->best = $rchildren[5]->plaintext;
			$tmp->last = $rchildren[6]->plaintext;
			$tmp->avg = $rchildren[7]->plaintext;
			$tmp->class = $rchildren[8]->plaintext;
			$tmp->rankc = intval($rchildren[9]->plaintext);
			$tmp->group = $rchildren[10]->plaintext;
			$tmp->rankg = intval($rchildren[11]->plaintext);
			$tmp->stops = intval($rchildren[12]->plaintext);
			$res[] = ($tmp);	
		}
	}
	
	echo json_encode($res);
?>