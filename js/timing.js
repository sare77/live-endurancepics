function loadTiming(bClass, bStorage, bRefresh) {	
	$("#loader").show();
	if(bStorage == undefined) {
		bStorage = false;
	}
	
	if(bRefresh == undefined) {
		bRefresh = false;
	}
	
	if(bStorage == true) {
		if (bClass == undefined || bRefresh == true || sessionStorage.data == undefined) {
			$.ajax({ 
				url: "svc/getTiming.php", 
				async: false,
				success: function (data) { 
					sessionStorage.data = data; 
				}
			});
		}
		var data = $.parseJSON(sessionStorage.data);
		showData(data, bClass);
	} else {	
		$.getJSON("svc/getTiming.php", function (data) {
			showData(data, bClass);
		});
	}
}

function showData(data, bClass) {
	
	var b;
	var tempBest = 100000;
	var classes = new Array();
	$("table#ltTable").empty();
	$("table#ltTable").append(getTableHeader());
	$("#ddlClasses").empty();
	
		
	for(i=0;i<data.length;i++) {
		var val = data[i];
					
		//Classes
		if($.inArray(val.class, classes) == -1) {
			classes.push(val.class);
		}
		
		if(bClass != undefined && bClass != "all") {
			$("#hfLastFilter").val(bClass);
			if (bClass == val.class) {
				$("table#ltTable").append(getEntry(val));
				createEntryModal(val);
				// Best Time
				var bSeconds = val.best.split(":");
				
				if(bSeconds.length < 3) {
					bSeconds = (bSeconds[0] * 60.00) + parseFloat(bSeconds[1]);
					if(bSeconds < tempBest) {
						tempBest = bSeconds;
						b = val;
					}
				}
			}
		} 
		else {
			$("#hfLastFilter").val("all");
			$("table#ltTable").append(getEntry(val));
			createEntryModal(val);
			// Best Time
			var bSeconds = val.best.split(":");
			
			if(bSeconds.length < 3) {
				bSeconds = (bSeconds[0] * 60.00) + parseFloat(bSeconds[1]);
				if(bSeconds < tempBest) {
					tempBest = bSeconds;
					b = val;
				}
			}
		}
	}
	
	var strBest = '<h3><a href="#' + b.number + '">#' + b.number + ' ' + b.name + ' - ' + b.best + ' - ' + b.class + '</a></h3>';
	
	$("#best").html(strBest);
	$("#ddlClasses").append('<li><a href="#" class="class">all</a></li>');
	
	$.each(classes, function (index, val) {
		$("#ddlClasses").append('<li><a href="#" class="class">' + val + '</a></li>');
	});
	
	 $(".class").click(function () { 
		var bStorage = supportsStorage();
		 loadTiming($(this).text(), bStorage, false); 
	 });
    $("#loader").hide();
}

function getEntry(val) {
	var str = '<tr id="' + val.number + '">';
	str += '<td>' + val.position + "</td>";
	str += '<td><a data-toggle="modal" href="#m'+ val.number + '" class="btn btn-info btn-sm">' + val.number + "</a></td>";
	str += "<td>" + val.name + "</td>";
	str += "<td>" + val.laps + "</td>";
	str += "<td>" + val.gap + "</td>";
	str += "<td>" + val.stops + "</td>";
	str += '<td class="hidden-xs">' + val.best + "</td>";
	str += '<td class="hidden-xs hidden-sm">' + val.class + "</td>";
	str += '<td class="hidden-xs hidden-sm">' + val.rankc + "</td>";
	str += '<td class="hidden-xs hidden-sm">' + val.group + "</td>";
	str += '<td class="hidden-xs hidden-sm">' + val.rankg + "</td>";
	str += '<td class="hidden-xs hidden-sm">' + '<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.endurancepics.de/vln#' + val.number + '" data-text="P' + val.position + ' - #' + val.number + ' ' + val.name + ' - ' + val.class + '" data-lang="de" data-count="none" data-hashtags="vln">tweet position</a></td>';
	str += "</tr>";
	return str;
}

function getTableHeader() {
		var str = "<tr>";
		str += "<td>Pos</td>";
		str += "<td>Nb.</td>";
		str += "<td>Name</td>";
		str += "<td>Laps</td>";
		str += "<td>Gap</td>";
		str += "<td>Stops</td>";
		str += '<td class="hidden-xs">Best</td>';
		str += '<td class="hidden-xs hidden-sm">Class</td>';
		str += '<td class="hidden-xs hidden-sm">Rank Class</td>';
		str += '<td class="hidden-xs hidden-sm">Group</td>';
		str += '<td class="hidden-xs hidden-sm">Rank Group</td>';
		str += '<td class="hidden-xs hidden-sm">Tweet</td>';
		str += "</tr>";
		return str;
}

function createEntryModal(arg) {
	var str = '<div class="modal fade" id="m' + arg.number + '" tabindex="-1" role="dialog" aria-labelledby="m' + arg.number + 'Label" aria-hidden="true">';
	  str += '<div class="modal-dialog">';
	  str += '<div class="modal-content">';
	  str += '<div class="modal-header">';
	  str += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
	  str += '<h4 class="modal-title">#' + arg.number + ' - ' + arg.name + ' - ' + arg.class + '</h4>';
	  str += '</div>';
	  str += '<div class="modal-body">';
	  str += '<div class="col-md-6">';
	  str += '<strong>Pos:</strong> ' + arg.position + '<br />';
	  str += '<strong>Pos. Class:</strong> ' + arg.rankc + '<br />';
	  str += '<strong>Pos. Group:</strong> ' + arg.rankg;
	  str += '</div>';
	  str += '<div class="col-md-6">';
	  str += '<strong>Gap:</strong> ' + arg.gap + '<br />';
	  str += '<strong>Best:</strong> ' + arg.best;
	  str += '</div>';
	  str += '</div>';
	  str += '<div class="modal-footer">';
	  str += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	  str += '</div>';
	  str += '</div><!-- /.modal-content -->';
	  str += '</div><!-- /.modal-dialog -->';
	  str += '</div><!-- /.modal -->';
	  
	  $("#modals").append(str);
}

function supportsStorage() {
	if(typeof(Storage)!=="undefined")
	  {
		return true;
	  }
	else
	  {
		return false;
	  }
}
